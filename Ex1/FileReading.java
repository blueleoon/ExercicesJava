import java.io.*;
import java.util.*;

public class FileReading {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un chemin d'accès en anglais :");
        String path = sc.nextLine();
        File f = new File( path );

        // Récupere le chemin d'acces dans une file


        if (f.exists()){

            //Conditions fichier / dossier

            System.out.println("Le répertoire ou fichier existe ");
            if ( f.isDirectory()){
                System.out.println("C'est un répertoire ");
            } else {
                System.out.println("C'est un fichier ");

            }
        }
        else {
            System.out.println("Rien n'existe a cet emplacement");

        }
    }
}
