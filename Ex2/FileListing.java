import java.io.*;
import java.util.*;

public class FileListing {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un chemin d'accès de dossier en anglais :");
        String path = sc.nextLine();
        File chemin = new File( path );

        // Récupere le chemin d'acces dans une file

        if (chemin.exists() && chemin.isDirectory()) {

            //Si la location existe et que c'est un dossier

            File[] filesList = chemin.listFiles();

            //Cree un array file qui contient la liste des fichiers

            for(File f : filesList){
                if(f.isDirectory())
                    System.out.println(f.getName() + "/");

                // Si c'est un dossier, ajoute un slash pour différentier

                if(f.isFile()){
                    System.out.println(f.getName());
                }
            }
        } else {
            System.out.println("Rien n'existe a cet emplacement ou ce n'est pas un dossier");

        }
    }
}
