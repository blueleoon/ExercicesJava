
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.imageio.*;

public class DownloadPicture {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un url de photo :");
        String source = sc.nextLine();
        System.out.println("Veuillez saisir un nom de fichier avec .jpg (avec chemin d'acces en option):");
        String dest = sc.nextLine();

        try {
            URL url = new URL(source);

            //variable url avec l'url

            BufferedImage img = ImageIO.read(url);

            //permet de lire l'url et de le mettre en tant que input

            File file = new File(dest);
            ImageIO.write(img, "png", file);

            //ecrit dans le fichier l'image

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
