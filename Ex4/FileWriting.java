import java.io.*;
import java.util.*;

public class FileWriting {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Veuillez saisir un chemin d'accès de fichier en anglais avec ext:");
            String path = sc.nextLine();
            File chemin = new File(path);
            System.out.println("Veuillez saisir du texte :");

            FileWriter nouveauFichier = new FileWriter(path );

            while (true) {
                String text = sc.nextLine();

                //recupere ligne a ligne le texte


                if (text.equalsIgnoreCase("quit")){

                    //condition pour quitter le programme

                    break;
                }
                nouveauFichier.write(text + "\n");

                //ecrit dans le fichier ligne a ligne avec le \n

            }
            nouveauFichier.close();

            //ferme la connection pour sauvegarder

        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
