import java.io.*;
import java.util.*;

public class ListingExe {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un chemin d'accès en anglais :");
        String path = sc.nextLine();
        File chemin = new File( path );

        // Récupere le chemin d'acces dans une file

        if (chemin.exists() && chemin.isDirectory()) {

            File[] filesList = chemin.listFiles();
            for(File f : filesList){
                String extension = "";
                extension = f.getName().substring(f.getName().lastIndexOf('.')+1);

                //extension prend tout ce qu'il y apres le dernier point

                if(f.isFile() && extension.equals("bin")){

                    //verifie toute les extensions bin

                    System.out.println(f.getName());
                }
            }
        } else {
            System.out.println("Rien n'existe a cet emplacement ou ce n'est pas un dossier");

        }
    }
}
