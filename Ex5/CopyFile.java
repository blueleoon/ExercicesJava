

import java.io.*;
import java.util.*;

public class CopyFile {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Veuillez saisir un chemin d'accès de fichier en anglais avec ext:");
            String source = sc.nextLine();
            System.out.println("Veuillez saisir une destination :");
            String dest = sc.nextLine();

            InputStream input = null;
            OutputStream output = null;

            //initialise les streams en tant que variable

            try {
                input = new FileInputStream(source);
                output = new FileOutputStream(dest);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = input.read(buffer)) > 0) {
                    output.write(buffer, 0, length);

                    //copie dans le fichier des parties du buffer les un les autres a la suite

                }
            } finally {
                input.close();
                output.close();

                //ferme les connections

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
